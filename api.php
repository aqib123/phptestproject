<?php
use Aws\Sdk;
use Aws\S3\S3Client;
use Aws\Lambda\LambdaClient;
use Controller\LoginController;
use Controller\RegisterController;

require "bootstrap.php";
require "aws-config.php";
require "database-connection.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/api.php/', $uri );

// all of our endpoints start with /person
// everything else results in a 404 Not Found
$requestMethod = $_SERVER["REQUEST_METHOD"];
$bodyData = $_POST;

// Temporary Change 
$functionCall = $_POST['mehtodCall'];

$queryData = $_GET;

$lambdaClient = LambdaClient::factory($config);
switch ($functionCall) {
    case 'login':
        $controller = new LoginController($requestMethod, $bodyData, $queryData, $lambdaClient, $mysqli);
		$controller->processRequest();
		break;
	case 'register':
		$controller = new RegisterController($requestMethod, $bodyData, $queryData, $lambdaClient, $mysqli);
		$controller->processRequest();
		break;
    default:
        break;
}