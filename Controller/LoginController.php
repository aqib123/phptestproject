<?php
namespace Controller;
error_reporting(E_ALL);
ini_set('display_errors', 'On');
// Instantiate an Amazon S3 client.

class LoginController {

    private $requestMethod;
    private $bodyData;
    private $queryData;
    private $lambdaClient;
    private $con;

    public function __construct($requestMethod, $bodyData, $queryData, $lambdaClient, $dbConnection)
    {
        $this->requestMethod = $requestMethod;
        $this->bodyData = $bodyData;
        $this->queryData = $queryData;
        $this->lambdaClient = $lambdaClient;
        $this->con = $dbConnection;
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                echo 'GET REQUEST';
                break;
            case 'POST':
                $response = $this->login($this->bodyData);
                break;
            case 'PUT':
               	echo 'PUT REQUEST';
                break;
            case 'DELETE':
                echo "DELETE REQUEST";
                break;
            default:
                echo 'DEFAULT REQUEST';
                break;
        }
        echo $response;
    }

    private function login($record)
    {
    	try {
            if ($record['email'] == '' &&  $record['password'] == '')
            {
                return json_encode(['status' => false, 'msg' => 'Field data is required']);
            }
            $email = $this->con->real_escape_string($record['email']);
            $sql = "SELECT password FROM users WHERE email = '$email'";
            $password = null;
            $result = $this->con->query($sql);
            $row = $result->fetch_assoc();
            if (empty($row)) {
                return json_encode(['status' => false, 'msg' => 'User Not Found']);
            }
            $result->free_result();
            $this->con->close();
    		$str = '{"Type":"User"}';
    		$base64str = base64_encode($str); 
        	$result = $this->lambdaClient->invoke([
			    // The name your created Lamda function
			    'FunctionName' => 'verify-hash',
			    'ClientContext' => $base64str,
			    'Payload' => json_encode(['stringPassword' => $record['password'], 'hashPassword' => $row['password']])
			]);
			$response = json_decode($result->get('Payload'), true);
			if (isset($response['errorMessage']))
			{
                $msg = json_decode($response['errorMessage'], true);
                if ($msg['status']) {
                    return json_encode(['status' => true, 'msg' => 'Successfully Logged in']);
                }
				return json_encode(['status' => false, 'msg' => $msg['msg']]);
			}
			// if (isset($response['msg']) && $response['msg'] == 'Success')
			// {

			// }
		}
		catch(Exception $e) {
			return json_encode(['status' => false, 'msg' => 'Invoking Function Error']);
		}
    }
}