<?php
namespace Controller;
error_reporting(E_ALL);
ini_set('display_errors', 'On');
// Instantiate an Amazon S3 client.

class RegisterController {

    private $requestMethod;
    private $bodyData;
    private $queryData;
    private $lambdaClient;
    private $con;

    public function __construct($requestMethod, $bodyData, $queryData, $lambdaClient, $dbConnection)
    {
        $this->requestMethod = $requestMethod;
        $this->bodyData = $bodyData;
        $this->queryData = $queryData;
        $this->lambdaClient = $lambdaClient;
        $this->con = $dbConnection;
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                echo 'GET REQUEST';
                break;
            case 'POST':
                $response = $this->register($this->bodyData);
                break;
            case 'PUT':
               	echo 'PUT REQUEST';
                break;
            case 'DELETE':
                echo "DELETE REQUEST";
                break;
            default:
                echo 'DEFAULT REQUEST';
                break;
        }
        echo $response;
    }

    private function register($record)
    {
    	try {
            if ($record['email'] == '' &&  $record['password'] == '') {
                return json_encode(['status' => false, 'msg' => 'Field data is required']);
            }
            $email = $this->con->real_escape_string($record['email']);
            $name = $this->con->real_escape_string($record['name']);
            $password = $this->con->real_escape_string($record['password']);
            $currentDate = date('Y-m-d');

            /*----------  Check if Email Already Exist  ----------*/
            $userExist = "SELECT id, email FROM users WHERE email = '$email'";
            $result = $this->con->query($userExist);
            $row = $result->fetch_assoc();
            $result->free_result();
            if (!empty($row)) {
                return json_encode(['status' => false, 'msg' => 'Email already exist']);
                $this->con->close();
            }
            /*----------  End of User Exist Validation  ----------*/            
    		$str = '{"Type":"User"}';
    		$base64str = base64_encode($str); 
        	$resultRecord = $this->lambdaClient->invoke([
			    // The name your created Lamda function
			    'FunctionName' => 'convert-to-hash',
			    'ClientContext' => $base64str,
			    'Payload' => json_encode(['email' => $record['email'], 'password' => $record['password']])
			]);
			$response = json_decode($resultRecord->get('Payload'), true);
            // print_r($response);
            // die();
			if (isset($response['errorMessage']))
			{
				$msg = json_decode($response['errorMessage'], true);
                if ($msg['status']) {
                    $hashPassword = $msg['passwordHash'];
                    $sql = "INSERT INTO users (email, name, password, created_at, updated_at) VALUES ('$email','$name','$hashPassword', '$currentDate', '$currentDate')";
                    if ($this->con->query($sql)) {
                       $this->con->close(); 
                       return json_encode(['status' => true, 'msg' => 'Successfully Registered']);
                    }
                    if ($this->con->errno) {
                       $this->con->close();
                       return json_encode(['status' => false, 'msg' => 'Database Insertion Error']);
                    }
                }
				return json_encode(['status' => false, 'msg' => $msg['msg']]);
			}
			// if (isset($response['msg']) && $response['msg'] == 'Success')
			// {

			// }
		}
		catch(Exception $e) {
			return json_encode(['status' => false, 'msg' => 'Invoking Function Error']);
		}
    }
}