<?php
require 'config/database.php';

$mysqli = new mysqli($db['DBHOST'],$db['DBUSER'],$db['DBPASSWORD'],$db['DBDATABASE'], $db['DBPORT']);

if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}
