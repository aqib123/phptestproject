<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<!------ Include the above in your HEAD tag ---------->

<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Registration form</h3>
            <div class="container">
            <div class="alert alert-success" style="display: none">
                <dir class="message">
                    <!-- <strong>Success!</strong> Indicates a successful or positive action. -->
                </dir>
            </div>
            <div class="alert alert-danger" style="display: none">
                <div class="message">
                    <strong>Success!</strong> Indicates a successful or positive action.
                </div>
            </div>
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <div class="form-group">
                                <label for="name" class="text-info">Name:</label><br>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="username" class="text-info">Email:</label><br>
                                <input type="email" name="email" id="emai" class="form-control" required>
                                <input type="hidden" name="mehtodCall" value="register">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                                <a href="index.php" class="btn btn-success">Login</a>
                                <img class="loader" src="assets/images/loading.gif" style="height: 32px; display: none;">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('doucment').ready(function(){
            $("form").submit(function(event){
                $(".loader").show();
                $('.alert-success').hide()
                $('.alert-danger').hide()
                event.preventDefault();
                // Abort any pending request
                var $form = $(this);
                var serializedData = $form.serialize();
                // Fire off the request to /form.php
                $.ajax({
                    url: "api.php",
                    type: "post",
                    data: serializedData,
                    success: function (response) {
                        if (response.status) {
                            $('.alert-success .message').html(response.msg)
                            $('.alert-success').show()
                        } else {
                            $('.alert-danger .message').html(response.msg)
                            $('.alert-danger').show()

                        }    
                        $(".loader").hide();    
                    // You will get response from your PHP page (what you echo or print)
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $(".loader").hide();
                        console.log(textStatus, errorThrown);
                    }
                });
            });
        })
    </script>
</body>
