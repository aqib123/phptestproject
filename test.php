<?php

use Aws\Sdk;
use Aws\S3\S3Client;
use Aws\Lambda\LambdaClient;
use Controller\LoginController;

require "bootstrap.php";
require "aws-config.php";
$lambdaClient = LambdaClient::factory($config);
try {
        	$result = $lambdaClient->invoke([
			    // The name your created Lamda function
			    'FunctionName' => 'hello-world',
			]);
			die('here');	
		}
		catch(Exception $e) {
			echo json_encode(['status' => false, 'msg' => 'Invoking Function Error']);
		}
// try {
// 	$result = $client->invoke([
// 	    // The name your created Lamda function
// 	    'FunctionName' => 'hello-world',
// 	]);
// 	// json_decode((string) $result->get('Payload'));
// 	var_dump($result);
// 	die();	
// }
// catch(Exception $e) {
// 	echo 'error';
// }